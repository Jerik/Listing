# A menu interface for conveniently using the features of Listing via the command line

import re
import os
import sys
import pickle
import base64
import binascii
from copy import deepcopy
from listing import Listing
from collections import OrderedDict


def get_nonempty_input(input_message):
    """Ask for user input with 'input_message' and check whether it contains at least one non-whitespace
    character. If so, return it. Otherwise print a warning_message and repeat the process."""
    print()
    while True:
        user_input = input(input_message).strip()
        if re.fullmatch(r".*\S.*", user_input):
            return user_input
        else:
            print("Please enter something.")


def get_valid_integral_input(lower_bound, upper_bound, input_message=None):
    """Ask for user input with check whether it is an integer between 'lower_bound' and 'upper_bound' (bounds that
    equal None are omitted). If so, return it. Otherwise print a warning_message and repeat the process.
    If 'input_message' isn't specified, it will default to "Input: "."""
    if input_message is None:
        input_message = "Input: "
    if lower_bound is not None and upper_bound is not None:
        warning_message = "Please enter an integer between {} and {}.".format(lower_bound, upper_bound)
    elif lower_bound is not None and upper_bound is None:
        warning_message = "Please enter an integer greater or equal {}.".format(lower_bound)
    elif lower_bound is None and upper_bound is not None:
        warning_message = "Please enter an integer less or equal {}.".format(upper_bound)
    else:
        warning_message = "Please enter an integer."
    while True:
        user_input = input(input_message).strip()
        try:
            num = int(user_input)
        except ValueError:
            print(warning_message)
            continue
        if lower_bound is not None and num < lower_bound or upper_bound is not None and num > upper_bound:
            print(warning_message)
            continue
        return num


def process_menu(menu_options, highlighted_option=None):
    """Print a menu with the options from 'menu_options' and return the choice of the user as a list index for
    'menu_options'. A 'highlighted_option' can be supplied, which will be displayed as the last option.
    If this option is chosen, return None."""
    if highlighted_option is None:
        for (i, option) in enumerate(menu_options):
            print("{}) {}".format(i + 1, option))
        return get_valid_integral_input(1, len(menu_options)) - 1
    else:
        highlighted_position = len(menu_options) + 1
        for (i, option) in enumerate(menu_options):
            print("  {}) {}".format(i + 1, option))
        print("# {}) {}".format(highlighted_position, highlighted_option))
        user_input = get_valid_integral_input(1, highlighted_position)
        if user_input == highlighted_position:
            return None
        else:
            return user_input - 1


def get_selected_options(possible_options, option_type):
    """Ask user to select options from 'possible_options' until the user is satisfied or all
    options have been selected, and return them. At least one option has to be selected."""
    if not possible_options:
        print("\nThere is no {} that can be selected.".format(option_type))
        return []
    selected_options = []
    while True:
        # Gather options that haven't been selected already
        menu_options = [option for option in possible_options if option not in selected_options]
        # After at least one sorting category has been selected, add an option to end category selection
        highlighted_option = "DONE" if selected_options else None
        # Print menu for selecting the next option, and proceed according to user input
        print("\nAdd a {}:\n".format(option_type))
        choice = process_menu(menu_options, highlighted_option=highlighted_option)
        if choice is None:
            break
        else:
            selected_options.append(menu_options[choice])
            if len(selected_options) == len(possible_options):
                print("\nEvery {} has been selected.".format(option_type))
                break
    return selected_options


def quicksearch_item_checker(item, container, item_type, check_inside, print_message=lambda x: True):
    """Item checker with a search feature: If 'check_inside' equals True, 'print_message' returns True and 'item'
    is not in 'container', look for items in 'container' that include 'item' and enable the user to choose one of
    those items instead. Apart from that, the functionality is the same as the standard item checker."""
    if check_inside:
        search_results = []
        for container_item in container:
            if container_item == item:
                return item
            if item in container_item:
                search_results.append(container_item)
        if print_message(item):
            print("\nThe {} \"{}\" doesn't exist.".format(item_type, item))
            if search_results:
                print("Try one of these instead:")
                choice = process_menu(search_results, highlighted_option="NONE")
                if choice is not None:
                    return search_results[choice]
        return None
    else:
        if item not in container:
            return item
        else:
            if print_message(item):
                print("\nThe {} \"{}\" already exists.".format(item_type, item))
            return None


class ListingMenu:
    """A menu for accessing the features of Listing with the abilities
    to lock items and save/load instances of Listing to/from files."""

    def __init__(self):
        self._current_listing = None      # The currently opened Listing that can be edited
        self._unchanged_listing = None    # A copy of the latest saved version of the current Listing
        self._listing_filepath = None     # The filepath of the save file of the current Listing

        # Locked items that are used by default if an item of the concerned type is
        # needed while editing the Listing (expect when trying to delete an item)
        self._locked_items = OrderedDict([(item_type, None) for item_type in ["object", "category", "tag", "image"]])

    def _save(self):
        if self._listing_filepath is None:
            # Redirect to "Save as" because the Listing hasn't been saved yet
            self._save_as()
        else:
            # Pickle Listing into a byte object, encode it and write it to the save file
            pickled_listing = pickle.dumps(self._current_listing, protocol=pickle.HIGHEST_PROTOCOL)
            encoded_listing = base64.b64encode(pickled_listing)
            with open(self._listing_filepath, "wb") as file:
                file.write(encoded_listing)
            # Update the local variable storing the latest saved version of the Listing
            self._unchanged_listing = deepcopy(self._current_listing)

    def _save_as(self):
        while True:
            filepath = input("\nEnter filepath of save file: ").strip()
            if os.path.isfile(filepath):
                # Ask before overwriting an existing file
                print("\nOverwrite existing file (at) \"{}\"?\n1) Yes  2) No".format(filepath))
                if get_valid_integral_input(1, 2) == 2:
                    continue
            # Pickle Listing into a byte object, encode it and write it to a save file if the filepath is valid
            pickled_listing = pickle.dumps(self._current_listing, protocol=pickle.HIGHEST_PROTOCOL)
            encoded_listing = base64.b64encode(pickled_listing)
            try:
                with open(filepath, "wb") as file:
                    file.write(encoded_listing)
            except OSError:
                print("\nFilepath is invalid.")
                continue
            else:
                # Update the local variables, which store the latest saved
                # version of the Listing and the filepath of the save file
                self._unchanged_listing = deepcopy(self._current_listing)
                self._listing_filepath = filepath
                break

    def _load(self):
        filepath = input("\nEnter filepath of save file: ").strip()
        if os.path.isfile(filepath):
            # Read the file data and try to decode and unpickle it
            with open(filepath, "rb") as file:
                file_data = file.read()
            try:
                decoded_data = base64.b64decode(file_data)
                unpickled_data = pickle.loads(decoded_data)
            except (binascii.Error, pickle.UnpicklingError):
                print("\nFile is invalid or corrupted.\n")
                return
            if isinstance(unpickled_data, Listing):
                # Set the current and unchanged Listing to the Listing from the file, set
                # the filepath value, unlock locked items and open the Listing main menu
                self._current_listing = deepcopy(unpickled_data)
                self._unchanged_listing = deepcopy(unpickled_data)
                self._listing_filepath = filepath
                for item_type in self._locked_items:
                    self._locked_items[item_type] = None
                self._listing_main_menu()
            else:
                print("\nFile is invalid.\n")
        else:
            print("\nFile doesn't exist.\n")

    def _check_safe_exit(self):
        """Check if there are any unsaved changes to the Listing and if so, ask the user how to proceed.
        Return True if the exit is permitted. Otherwise return False."""
        if self._current_listing == self._unchanged_listing:
            # Permit exit
            return True
        print("\nThere are unsaved changes to the current Listing.\n1) Save  2) Don't save  3) Cancel")
        choice = get_valid_integral_input(1, 3)
        if choice == 1:
            # Save unsaved changes and permit exit
            self._save()
            return True
        elif choice == 2:
            # Reset Listing to latest saved version and permit exit
            # For Listings that haven't been saved yet, this will be None
            self._current_listing = deepcopy(self._unchanged_listing)
            return True
        elif choice == 3:
            # Stop exit
            return False

    def _check_locked_item(self, item_type, default=None):
        """Check whether the locked item with the specified 'item_type' (either "object", "category", "tag" or "image")
        still exists within the corresponding container of the Listing. If it doesn't, it will be set to 'default'."""
        containers = {"object": self._current_listing.objects, "category": self._current_listing.category_names,
                      "tag": self._current_listing.tags, "image": self._current_listing.images}
        if self._locked_items[item_type] is not None and self._locked_items[item_type] not in containers[item_type]:
            self._locked_items[item_type] = default

    def _get_item_name(self, item_type, input_message):
        """Check whether the specified 'item_type' (either "object", "category", "tag" or "image") has a locked item.
        If so, return its name. Otherwise ask the user with 'input_message' for a name and return it."""
        locked_item = self._locked_items[item_type]
        if locked_item is not None:
            item_name = locked_item if type(locked_item) is str else locked_item.name
            print("\nLocked {} \"{}\" is used.".format(item_type, item_name))
        else:
            item_name = input(input_message).strip()
        return item_name

    def _print_menu_header(self):
        """Print the name of the current Listing and the names of potential locked items as a menu header."""
        print("\n\nListing name: {}\n".format(self._current_listing.name))
        # Create a list of tuples that stores existing locked items and their item descriptions
        existing_locked_items = [("Locked {}".format(item_type), locked_item) for (item_type, locked_item)
                                 in self._locked_items.items() if locked_item is not None]
        # The column width used for the item description is determined by the longest description
        item_desc_col_width = max([len(item_description) for (item_description, _) in existing_locked_items], default=0)
        # Print names of existing locked items
        for (item_description, locked_item) in existing_locked_items:
            item_name = locked_item if type(locked_item) is str else locked_item.name
            print("{:<{}}: {}".format(item_description, item_desc_col_width, item_name))
        if existing_locked_items:
            print()

    def _print_sorted_objects(self, obj_list=None):
        if obj_list is None:
            obj_list = self._current_listing.objects
        # Possible sorting categories are the object name, all categories, and potentially also aggregate and average
        max_sorting_ctgr_number = len(self._current_listing.category_names) + 1
        if self._current_listing.display_aggr_avrg and any([obj.aggr_avrg is not None for obj in obj_list]):
            max_sorting_ctgr_number += 2
        # Ask for sorting categories until the user is satisfied or all possible ones have been selected
        sorting_ctgr_ids = OrderedDict()
        while True:
            # Gather menu options that haven't been selected already
            menu_options = []
            if "Object name" not in sorting_ctgr_ids:
                menu_options.append("Object name")
            menu_options.extend([i for i in range(len(self._current_listing.category_names)) if i not in sorting_ctgr_ids])
            if self._current_listing.display_aggr_avrg and any([obj.aggr_avrg is not None for obj in obj_list]):
                if "Aggregate" not in sorting_ctgr_ids:
                    menu_options.append("Aggregate")
                if "Average" not in sorting_ctgr_ids:
                    menu_options.append("Average")
            # After at least one sorting category has been selected, add an option to end category selection
            highlighted_option = "DONE" if sorting_ctgr_ids else None
            # Print menu for selecting the next sorting category and proceed according to user input
            print("\nAdd a sorting category (priority {}):\n".format(len(sorting_ctgr_ids) + 1))
            option_names = [dict(enumerate(self._current_listing.category_names))
                            .get(option, option) for option in menu_options]
            choice = process_menu(option_names, highlighted_option=highlighted_option)
            if choice is None:
                break
            else:
                print("\nSort in descending or ascending order?\n1) Descending  2) Ascending")
                reverse = get_valid_integral_input(1, 2) == 1
                sorting_ctgr_ids[menu_options[choice]] = reverse
                if len(sorting_ctgr_ids) == max_sorting_ctgr_number:
                    print("\nEvery sorting category has been selected.")
                    break
        # Sort objects and print them in the sorted order
        sorted_objects = self._current_listing.get_sorted_objects(sorting_ctgr_ids, obj_list=obj_list)
        self._current_listing.print_objects(obj_list=sorted_objects)

    def _search_objects(self, obj_list=None):
        possible_areas = ["Object names", "Category entries", "Tags", "Image names"]
        print("\nSearch in all areas?\n1) Yes  2) No")
        if get_valid_integral_input(1, 2) == 1:
            # Use all possible search areas and categories
            search_areas = possible_areas
            search_ctgrs = list(self._current_listing.category_names)
        else:
            # Ask user for search areas
            search_areas = get_selected_options(possible_areas, "search area")
            search_ctgrs = []
            if "Category entries" in search_areas:
                print("\nSearch in entries of all categories?\n1) Yes  2) No")
                if get_valid_integral_input(1, 2) == 1:
                    # Use all possible search categories
                    search_ctgrs = list(self._current_listing.category_names)
                else:
                    # Ask user for search categories
                    search_ctgrs = get_selected_options(list(self._current_listing.category_names), "category")
        # Ask for a search term as well as a strictness level, and search in the selected areas
        search_term = input("\nSearch term: ")
        print("\nPlease select a strictness level for the search:\n1) Case-insensitive containing matches")
        print("2) Case-sensitive containing matches\n3) Exact matches")
        strictness_level = get_valid_integral_input(1, 3) - 1
        search_results = self._current_listing.get_search_results(search_areas, search_ctgrs, search_term,
                                                                  strictness_level, obj_list=obj_list)
        print("\nYour search request has {} result(s).".format(len(search_results)))
        if search_results:
            # Ask how to proceed with the search results
            menu_options = ["Print search result(s)", "Add another search term"]
            if len(search_results) > 1:
                # Add options to sort or compare multiple search results
                menu_options.insert(1, "Print sorted search results")
                menu_options.insert(2, "Compare search results")
            print()
            choice = process_menu(menu_options)
            if menu_options[choice] == "Print search result(s)":
                self._current_listing.print_objects(obj_list=search_results)
            elif menu_options[choice] == "Print sorted search results":
                self._print_sorted_objects(obj_list=search_results)
            elif menu_options[choice] == "Compare search results":
                self._current_listing.print_chart(obj_list=search_results)
            elif menu_options[choice] == "Add another search term":
                self._search_objects(obj_list=search_results)

    def _compare_objects(self):
        if len(self._current_listing.objects) < 2:
            print("\nThere are not enough listed objects to make a comparison.")
            return
        print("\nCompare all objects?\n1) Yes  2) No")
        if get_valid_integral_input(1, 2) == 1:
            # Compare all objects
            self._current_listing.print_chart()
        else:
            self._current_listing.print_object_names()
            # Ask for objects to compare until the user is satisfied or until all possible ones have been selected
            objects_to_compare = []
            while True:
                obj_name = input("\nObject name: ").strip()
                obj = self._current_listing.get_object(obj_name, item_checker=quicksearch_item_checker)
                if obj is not None:
                    if obj in objects_to_compare:
                        print("\nThis object has already been selected.")
                    else:
                        objects_to_compare.append(obj)
                        if len(objects_to_compare) == len(self._current_listing.objects):
                            print("\nEvery object has been selected.")
                            break
                # After two or more objects have been selected, ask whether to add another one or not
                if len(objects_to_compare) >= 2:
                    print("\nAdd another object?\n1) Yes  2) No")
                    if get_valid_integral_input(1, 2) == 2:
                        break
            self._current_listing.print_chart(obj_list=objects_to_compare)

    def _edit_category_order(self):
        ctgr_number = len(self._current_listing.category_names)
        if ctgr_number < 2:
            print("\nThere are not enough categories to change their order.")
            return
        self._current_listing.print_category_names()
        ordered_ctgr_names = [None] * ctgr_number
        # Ask for new position of each category
        for ctgr_name in self._current_listing.category_names:
            print("\nNew position of \"{}\": ".format(ctgr_name))
            while True:
                choice = get_valid_integral_input(1, ctgr_number) - 1
                if ordered_ctgr_names[choice] is not None:
                    print("This position has already been picked.")
                else:
                    ordered_ctgr_names[choice] = ctgr_name
                    break
        self._current_listing.edit_category_order(ordered_ctgr_names)

    def listing_select_menu(self):
        while True:
            menu_options = ["Create new Listing", "Load Listing from file", "Quit"]
            if self._current_listing is not None:
                # Add option to return to the main menu of the latest saved version of the last opened Listing
                menu_options.insert(2, "Go to main menu of Listing \"{}\"".format(self._current_listing.name))
            choice = process_menu(menu_options)
            if menu_options[choice] == "Create new Listing":
                # Create a new instance of Listing, reset the unchanged Listing and the
                # filepath value, unlock locked items, and open the Listing main menu
                listing_name = get_nonempty_input("Enter name of new Listing: ")
                self._current_listing = Listing(listing_name)
                self._unchanged_listing = None
                self._listing_filepath = None
                for item_type in self._locked_items:
                    self._locked_items[item_type] = None
                self._listing_main_menu()
            elif menu_options[choice] == "Load Listing from file":
                self._load()
            elif menu_options[choice].startswith("Go to main menu of Listing"):
                self._listing_main_menu()
            elif menu_options[choice] == "Quit":
                sys.exit()

    def _listing_main_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Display Listing", "Edit Listing", "Edit settings", "Save",
                            "Save as", "Return to Listing select menu", "Quit"]
            choice = process_menu(menu_options)
            if menu_options[choice] == "Display Listing":
                self._listing_display_menu()
            elif menu_options[choice] == "Edit Listing":
                self._listing_edit_menu()
            elif menu_options[choice] == "Edit settings":
                self._settings_menu()
            elif menu_options[choice] == "Save":
                self._save()
            elif menu_options[choice] == "Save as":
                self._save_as()
            elif menu_options[choice] == "Return to Listing select menu" and self._check_safe_exit():
                print("\n")
                break
            elif menu_options[choice] == "Quit" and self._check_safe_exit():
                sys.exit()

    def _listing_display_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Print objects", "Print sorted objects", "Search objects", "Compare objects",
                            "Display images", "Print item names", "Return to Listing main menu"]
            choice = process_menu(menu_options)
            if menu_options[choice] == "Print objects":
                self._current_listing.print_objects()
            elif menu_options[choice] == "Print sorted objects":
                self._print_sorted_objects()
            elif menu_options[choice] == "Search objects":
                self._search_objects()
            elif menu_options[choice] == "Compare objects":
                self._compare_objects()
            elif menu_options[choice] == "Display images":
                self._images_display_menu()
            elif menu_options[choice] == "Print item names":
                self._item_names_menu()
            elif menu_options[choice] == "Return to Listing main menu":
                break

    def _images_display_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Display single image", "Display all images connected to an object",
                            "Display all images", "Return to Listing display menu"]
            choice = process_menu(menu_options)
            if menu_options[choice] == "Display single image":
                self._current_listing.print_image_names(show_obj_names=True)
                image_name = input("\nEnter image name: ").strip()
                self._current_listing.display_image(image_name, item_checker=quicksearch_item_checker)
            elif menu_options[choice] == "Display all images connected to an object":
                self._current_listing.print_object_names(show_image_names=True)
                obj_name = input("\nEnter object name: ").strip()
                self._current_listing.display_connected_images(obj_name, item_checker=quicksearch_item_checker)
            elif menu_options[choice] == "Display all images":
                self._current_listing.print_image_names(show_obj_names=True)
                self._current_listing.display_all_images()
            elif menu_options[choice] == "Return to Listing display menu":
                break

    def _item_names_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Print object names", "Print category names", "Print tags",
                            "Print image names", "Return to Listing display menu"]
            choice = process_menu(menu_options)
            if menu_options[choice] == "Print object names":
                self._current_listing.print_object_names()
            elif menu_options[choice] == "Print category names":
                self._current_listing.print_category_names()
            elif menu_options[choice] == "Print tags":
                print("\nShow names of respective tagged objects?\n1) Yes  2) No")
                show_obj_names = get_valid_integral_input(1, 2) == 1
                self._current_listing.print_tags(show_obj_names=show_obj_names)
            elif menu_options[choice] == "Print image names":
                print("\nShow names of respective connected objects?\n1) Yes  2) No")
                show_obj_names = get_valid_integral_input(1, 2) == 1
                self._current_listing.print_image_names(show_obj_names=show_obj_names)
            elif menu_options[choice] == "Return to Listing display menu":
                break

    def _listing_edit_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Edit items", "Edit category entry of object", "Toggle tag for object", "Toggle connection"
                            " of object and image", "(Un)lock items", "Edit Listing name", "Return to Listing main menu"]
            choice = process_menu(menu_options)
            if menu_options[choice] == "Edit items":
                self._items_edit_menu()
            elif menu_options[choice] == "Edit category entry of object":
                self._current_listing.print_object_names()
                self._current_listing.print_category_names()
                obj_name = self._get_item_name("object", "\nEnter object name: ")
                ctgr_name = self._get_item_name("category", "\nEnter category name: ")
                new_entry = input("\nEnter new category entry: ").strip()
                self._current_listing.edit_category_entry(obj_name, ctgr_name, new_entry,
                                                          item_checker=quicksearch_item_checker)
            elif menu_options[choice] == "Toggle tag for object":
                self._current_listing.print_object_names()
                self._current_listing.print_tags(show_obj_names=True)
                obj_name = self._get_item_name("object", "\nEnter object name: ")
                tag_name = self._get_item_name("tag", "\nEnter tag name: ")
                self._current_listing.toggle_tag(obj_name, tag_name, item_checker=quicksearch_item_checker)
            elif menu_options[choice] == "Toggle connection of object and image":
                self._current_listing.print_object_names()
                self._current_listing.print_image_names(show_obj_names=True)
                obj_name = self._get_item_name("object", "\nEnter object name: ")
                image_name = self._get_item_name("image", "\nEnter image name: ")
                self._current_listing.toggle_image_connection(obj_name, image_name,
                                                              item_checker=quicksearch_item_checker)
            elif menu_options[choice] == "(Un)lock items":
                self._items_lock_menu()
            elif menu_options[choice] == "Edit Listing name":
                self._current_listing.name = get_nonempty_input("Enter new Listing name: ")
            elif menu_options[choice] == "Return to Listing main menu":
                break

    def _items_edit_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Edit objects", "Edit categories", "Edit tags", "Edit images", "Return to Listing edit menu"]
            choice = process_menu(menu_options)
            if menu_options[choice] == "Edit objects":
                self._objects_edit_menu()
            elif menu_options[choice] == "Edit categories":
                self._categories_edit_menu()
            elif menu_options[choice] == "Edit tags":
                self._tags_edit_menu()
            elif menu_options[choice] == "Edit images":
                self._images_edit_menu()
            elif menu_options[choice] == "Return to Listing edit menu":
                break

    def _objects_edit_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Add object", "Rename object", "Delete object", "Return to items edit menu"]
            choice = process_menu(menu_options)
            if menu_options[choice] == "Add object":
                obj_name = get_nonempty_input("Enter name of new object: ")
                self._current_listing.add_object(obj_name)
            elif menu_options[choice] == "Rename object":
                self._current_listing.print_object_names()
                obj_name = self._get_item_name("object", "\nEnter current object name: ")
                new_obj_name = get_nonempty_input("Enter new object name: ")
                self._current_listing.rename_object(obj_name, new_obj_name, item_checker=quicksearch_item_checker)
            elif menu_options[choice] == "Delete object":
                self._current_listing.print_object_names()
                obj_name = input("\nEnter object name: ").strip()
                self._current_listing.delete_object(obj_name, item_checker=quicksearch_item_checker)
                self._check_locked_item("object")
            elif menu_options[choice] == "Return to items edit menu":
                break

    def _categories_edit_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Add category", "Rename category", "Delete category",
                            "Edit category order", "Return to items edit menu"]
            choice = process_menu(menu_options)
            if menu_options[choice] == "Add category":
                ctgr_name = get_nonempty_input("Enter name of new category: ")
                if self._current_listing.display_aggr_avrg:
                    print("\nUse new category for aggregate/average calculation?\n1) Yes  2) No")
                    use_for_calc = get_valid_integral_input(1, 2) == 1
                else:
                    use_for_calc = False
                self._current_listing.add_category(ctgr_name, use_for_calc)
            elif menu_options[choice] == "Rename category":
                self._current_listing.print_category_names()
                ctgr_name = self._get_item_name("category", "\nEnter current category name: ")
                new_ctgr_name = get_nonempty_input("Enter new category name: ")
                self._current_listing.rename_category(ctgr_name, new_ctgr_name, item_checker=quicksearch_item_checker)
                self._check_locked_item("category", default=new_ctgr_name)
            elif menu_options[choice] == "Delete category":
                self._current_listing.print_category_names()
                ctgr_name = input("\nEnter category name: ").strip()
                self._current_listing.delete_category(ctgr_name, item_checker=quicksearch_item_checker)
                self._check_locked_item("category")
            elif menu_options[choice] == "Edit category order":
                self._edit_category_order()
            elif menu_options[choice] == "Return to items edit menu":
                break

    def _tags_edit_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Add tag", "Rename tag", "Delete tag", "Return to items edit menu"]
            choice = process_menu(menu_options)
            if menu_options[choice] == "Add tag":
                tag_name = get_nonempty_input("Enter name of new tag: ")
                self._current_listing.add_tag(tag_name)
            elif menu_options[choice] == "Rename tag":
                self._current_listing.print_tags()
                tag_name = self._get_item_name("tag", "\nEnter current tag name: ")
                new_tag_name = get_nonempty_input("Enter new tag name: ")
                self._current_listing.rename_tag(tag_name, new_tag_name, item_checker=quicksearch_item_checker)
                self._check_locked_item("tag", default=new_tag_name)
            elif menu_options[choice] == "Delete tag":
                self._current_listing.print_tags()
                tag_name = input("\nEnter tag name: ").strip()
                self._current_listing.delete_tag(tag_name, item_checker=quicksearch_item_checker)
                self._check_locked_item("tag")
            elif menu_options[choice] == "Return to items edit menu":
                break

    def _images_edit_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Add image", "Rename image", "Delete image", "Return to items edit menu"]
            choice = process_menu(menu_options)
            if menu_options[choice] == "Add image":
                image_name = get_nonempty_input("Enter name of new image: ")
                print("\nLoad image from file or from URL?\n1) File  2) URL")
                is_filepath = get_valid_integral_input(1, 2) == 1
                image_source = input("\nEnter {} of the image: ".format("filepath" if is_filepath else "URL")).strip()
                self._current_listing.add_image(image_name, image_source, is_filepath)
            elif menu_options[choice] == "Rename image":
                self._current_listing.print_image_names()
                image_name = self._get_item_name("image", "\nEnter current image name: ")
                new_image_name = get_nonempty_input("Enter new image name: ")
                self._current_listing.rename_image(image_name, new_image_name, item_checker=quicksearch_item_checker)
                self._check_locked_item("image", default=new_image_name)
            elif menu_options[choice] == "Delete image":
                self._current_listing.print_image_names()
                image_name = input("\nEnter image name: ").strip()
                self._current_listing.delete_image(image_name, item_checker=quicksearch_item_checker)
                self._check_locked_item("image")
            elif menu_options[choice] == "Return to items edit menu":
                break

    def _items_lock_menu(self):
        while True:
            self._print_menu_header()
            menu_options = ["Lock object", "Lock category", "Lock tag", "Lock image", "Return to Listing edit menu"]
            for (item_type, locked_item) in self._locked_items.items():
                if locked_item is not None:
                    menu_options.insert(-1, "Unlock {}".format(item_type))
            choice = process_menu(menu_options)
            if menu_options[choice] == "Lock object":
                self._current_listing.print_object_names()
                obj_name = input("\nEnter name of new locked object: ").strip()
                obj = self._current_listing.get_object(obj_name, item_checker=quicksearch_item_checker)
                if obj is not None:
                    self._locked_items["object"] = obj
            elif menu_options[choice] == "Lock category":
                self._current_listing.print_category_names()
                ctgr_name = quicksearch_item_checker(input("\nEnter name of new locked category: ").strip(),
                                                     self._current_listing.category_names, "category", True)
                if ctgr_name is not None:
                    self._locked_items["category"] = ctgr_name
            elif menu_options[choice] == "Lock tag":
                self._current_listing.print_tags()
                tag_name = quicksearch_item_checker(input("\nEnter name of new locked tag: ").strip(),
                                                    self._current_listing.tags, "tag", True)
                if tag_name is not None:
                    self._locked_items["tag"] = tag_name
            elif menu_options[choice] == "Lock image":
                self._current_listing.print_image_names()
                image_name = quicksearch_item_checker(input("\nEnter name of new locked image: ").strip(),
                                                      self._current_listing.images, "image", True)
                if image_name is not None:
                    self._locked_items["image"] = image_name
            elif menu_options[choice].startswith("Unlock"):
                item_type = menu_options[choice][7:]
                self._locked_items[item_type] = None
            elif menu_options[choice] == "Return to Listing edit menu":
                break

    def _settings_menu(self):
        while True:
            print("\n\nSettings:")
            print("\nDisplay ranks of category entries: {}".format(self._current_listing.display_ranks))
            print("Display aggregate/average values : {}".format(self._current_listing.display_aggr_avrg))
            menu_options = ["Toggle display of ranks", "Toggle display of aggregate/average values",
                            "Return to Listing main menu"]
            if self._current_listing.display_aggr_avrg:
                if not any(self._current_listing.category_names.values()):
                    calc_ctgrs_status = "None"
                elif all(self._current_listing.category_names.values()):
                    calc_ctgrs_status = "All"
                else:
                    calc_ctgrs_status = "User-defined"
                print("Categories used for calculation  : {}".format(calc_ctgrs_status))
                print("Decimal places of average values : {}".format(self._current_listing.avrg_decimal_places))
                menu_options.insert(2, "Select categories for aggregate/average calculation")
                menu_options.insert(3, "Edit number of average value decimal places")
            print()
            choice = process_menu(menu_options)
            if menu_options[choice] == "Toggle display of ranks":
                self._current_listing.display_ranks = not self._current_listing.display_ranks
            elif menu_options[choice] == "Toggle display of aggregate/average values":
                self._current_listing.display_aggr_avrg = not self._current_listing.display_aggr_avrg
            elif menu_options[choice] == "Select categories for aggregate/average calculation":
                self._current_listing.print_category_names(only_calc_ctgrs=True)
                print("\nWhich categories are to be used for calculation?")
                print("1) All  2) None  3) Select custom categories  4) Keep current configuration")
                another_choice = get_valid_integral_input(1, 4)
                if another_choice == 1:
                    calc_ctgrs = list(self._current_listing.category_names)
                elif another_choice == 2:
                    calc_ctgrs = []
                elif another_choice == 3:
                    calc_ctgrs = get_selected_options(list(self._current_listing.category_names), "category")
                else:
                    continue
                self._current_listing.set_calculation_categories(calc_ctgrs)
            elif menu_options[choice] == "Edit number of average value decimal places":
                print()
                self._current_listing.avrg_decimal_places = get_valid_integral_input(
                    0, None, input_message="Enter new number of decimal places: ")
            elif menu_options[choice] == "Return to Listing main menu":
                break


if __name__ == "__main__":
    # Create a menu and open it
    menu = ListingMenu()
    menu.listing_select_menu()
