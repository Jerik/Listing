# A versatile listing with various features, including:
# - Editing objects, categories, tags and images
# - Printing objects and displaying images
# - Sorting, searching for and comparing objects
#
# Used versions of Python and external dependencies:
# - Python 3.6 (https://www.python.org/)
# - Pillow 5.2 (https://python-pillow.org/)
# - Requests 2.16 (http://docs.python-requests.org/en/master/)

import re
import os
import io
import requests
from PIL import Image
from collections import OrderedDict


def try_float_cast(string):
    """Return 'string' as float if possible. Otherwise return 'string' unchanged."""
    if not re.fullmatch(r"[+-]?[0-9]+(?:[.][0-9]+)?", string.strip()):
        # Prevent strings like "nan", "inf" or "1E6" from being returned as float
        # to avoid breaking aggregate/average calculation and causing confusion
        return string
    try:
        return float(string)
    except ValueError:
        return string


def natural_key(string):
    """Return a sorting key for 'string' that makes natural sorting of strings containing numbers possible."""
    return [try_float_cast(char_group) for char_group in re.split(r"([+-]?[0-9]+(?:[.][0-9]+)?)", string)]


def standard_item_checker(item, container, item_type, check_inside, print_message=lambda x: True):
    """If 'check_inside' equals True, check whether 'item' is in 'container'. Otherwise check whether 'item' is not
    in 'container'. In both cases, return 'item' if the corresponding boolean value equals True. Otherwise return None.
    The 'item_type' is used in a message that is printed if the return value is None and 'print_message' returns True."""
    if check_inside:
        if item in container:
            return item
        else:
            if print_message(item):
                print("\nThe {} \"{}\" doesn't exist.".format(item_type, item))
            return None
    else:
        if item not in container:
            return item
        else:
            if print_message(item):
                print("\nThe {} \"{}\" already exists.".format(item_type, item))
            return None


class ListedObject:
    """A single object that is stored in a Listing.
    
    Initializing arguments:
        name (str): name of the object
        ctgr_names (list of str): currently listed category names"""

    def __init__(self, name, ctgr_names):
        self.name = name
        self.ctgr_entries = {ctgr_name: "" for ctgr_name in ctgr_names}
        self.tags = []
        self.image_names = []

        # Aggregate and average values of pure number entries of specified categories
        # Takes the form of the tuple (aggregate, average), or None if the values can't be calculated
        self.aggr_avrg = None

    def calculate_aggregate_average(self, calc_ctgrs):
        """Calculate the aggregate and average values of the pure number entries of the categories specified in
        'calc_ctgrs'. This function needs to be called after an action is performed that could change those values."""
        aggregate = 0
        entry_number = 0
        for ctgr_name in calc_ctgrs:
            entry = try_float_cast(self.ctgr_entries[ctgr_name])
            if type(entry) is float:
                aggregate += entry
                entry_number += 1
        if entry_number > 0:
            self.aggr_avrg = (aggregate, aggregate / entry_number)
        else:
            self.aggr_avrg = None

    def get_aggregate(self):
        if self.aggr_avrg is not None:
            return self.aggr_avrg[0]
        else:
            return None

    def get_average(self):
        if self.aggr_avrg is not None:
            return self.aggr_avrg[1]
        else:
            return None

    # Define equality behaviour to allow equality tests

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self == other
        else:
            return NotImplemented


class Listing:
    """A listing with various features.

    Initializing argument:
        name (str): name of the listing"""

    def __init__(self, name):
        self.name = name
        self.objects = []
        self.category_names = OrderedDict()
        self.tags = []
        self.images = OrderedDict()
        self.display_ranks = False        # Determines whether the ranks of category entries are shown
        self.display_aggr_avrg = False    # Determines whether aggregate/average values are shown
        self.avrg_decimal_places = 2      # Number of decimal places that average values are displayed with

    def _get_rank(self, obj, sorting_ctgr_id, obj_list):
        """Return the rank of 'obj' in 'obj_list' after it has been sorted descendingly using 'sorting_ctgr_id',
        which can be either a category index derived from the list of category names, "Aggregate", or "Average".
        If 'sorting_ctgr_id' equals neither of those, a ValueError is raised. For a category, all entries in
        this category of the objects in 'obj_list' need to be convertible into float."""
        sorted_objects = self.get_sorted_objects(OrderedDict({sorting_ctgr_id: True}), obj_list)
        # Consider ties by using the rank of the highest ranked object with the same entry/aggregate/average value
        if type(sorting_ctgr_id) is int:
            ctgr_name = list(self.category_names)[sorting_ctgr_id]
            entry_value = float(obj.ctgr_entries[ctgr_name])
            objs_with_same_value = [obj for obj in obj_list if float(obj.ctgr_entries[ctgr_name]) == entry_value]
        elif sorting_ctgr_id == "Aggregate":
            aggr_value = obj.get_aggregate()
            objs_with_same_value = [obj for obj in obj_list if obj.get_aggregate() == aggr_value]
        elif sorting_ctgr_id == "Average":
            avrg_value = obj.get_average()
            objs_with_same_value = [obj for obj in obj_list if obj.get_average() == avrg_value]
        else:
            raise ValueError
        return min([sorted_objects.index(obj) for obj in objs_with_same_value]) + 1

    def _get_ctgr_entry_rank_info(self, obj, ctgr_name):
        """Return rank information string of the category entry of 'obj' in the category 'ctgr_name'.
        If ranks aren't displayed or there is no rank, return an empty string."""
        if self.display_ranks and type(try_float_cast(obj.ctgr_entries[ctgr_name])) is float:
            ranked_objs = [obj for obj in self.objects if type(try_float_cast(obj.ctgr_entries[ctgr_name])) is float]
            rank = self._get_rank(obj, list(self.category_names).index(ctgr_name), obj_list=ranked_objs)
            return " [{}/{}]".format(rank, len(ranked_objs))
        else:
            return ""

    def _get_aggr_avrg_rank_info(self, obj):
        """Return rank information strings of the aggregate and average values of 'obj' as a tuple.
        If ranks aren't displayed or 'obj' has no aggregate/average values, return empty strings."""
        if self.display_ranks and obj.aggr_avrg is not None:
            objs_with_aggr_avrg = [obj for obj in self.objects if obj.aggr_avrg is not None]
            aggr_rank = self._get_rank(obj, "Aggregate", obj_list=objs_with_aggr_avrg)
            avrg_rank = self._get_rank(obj, "Average", obj_list=objs_with_aggr_avrg)
            return (" [{}/{}]".format(aggr_rank, len(objs_with_aggr_avrg)),
                    " [{}/{}]".format(avrg_rank, len(objs_with_aggr_avrg)))
        else:
            return ("", "")

    def _get_max_ctgr_name_length(self, include_aggr):
        """Return the length of the longest category name. If aggregate and average are
        displayed and 'include_aggr' equals True, "Aggregate" is added as a category name."""
        lengths = [len(ctgr_name) for ctgr_name in self.category_names]
        if self.display_aggr_avrg and include_aggr:
            lengths.append(len("Aggregate"))
        return max(lengths, default=0)

    def _get_max_obj_length(self, obj):
        """Return the length of the longest category entry of 'obj', potentially including rank information,
        or the length of the name of 'obj' if it is longer than all of the category entries."""

        # The length of the object name is always included since it definitely exists
        lengths = [len(obj.name)]
        # Include the lengths of category entries with potential rank information
        for ctgr_name in self.category_names:
            lengths.append(len(obj.ctgr_entries[ctgr_name] + self._get_ctgr_entry_rank_info(obj, ctgr_name)))
        if self.display_aggr_avrg and obj.aggr_avrg is not None:
            # Include the lengths of aggregate and average values with potential rank information
            (aggr_rank_info, avrg_rank_info) = self._get_aggr_avrg_rank_info(obj)
            lengths.append(len(str(obj.get_aggregate()) + aggr_rank_info))
            lengths.append(len("{:.{}f}{}".format(obj.get_average(), self.avrg_decimal_places, avrg_rank_info)))
        return max(lengths)

    def _print_object(self, obj):
        """Print 'obj' with its name, category entries and potential additional information."""

        # Print object name
        print(obj.name)
        # Print category names and respective category entries with potential rank information
        ctgr_col_width = self._get_max_ctgr_name_length(include_aggr=obj.aggr_avrg is not None)
        for (ctgr_name, use_for_calc) in self.category_names.items():
            if self.display_aggr_avrg:
                # Print calculation indicator
                calc_indicator = "*" if use_for_calc else "-"
                print(calc_indicator, end=" ")
            print("{:<{}}: {}{}".format(ctgr_name, ctgr_col_width, obj.ctgr_entries[ctgr_name],
                                        self._get_ctgr_entry_rank_info(obj, ctgr_name)))
        if self.display_aggr_avrg and obj.aggr_avrg is not None:
            # Print aggregate and average values with potential rank information
            (aggr_rank_info, avrg_rank_info) = self._get_aggr_avrg_rank_info(obj)
            print("\n* {:<{}}: {}{}".format("Aggregate", ctgr_col_width, obj.get_aggregate(), aggr_rank_info))
            print("* {:<{}}: {:.{}f}{}".format("Average", ctgr_col_width, obj.get_average(),
                                               self.avrg_decimal_places, avrg_rank_info))
        if obj.tags:
            # Print tags that the object is tagged with
            print("\nTags: {}".format(", ".join(obj.tags)))
        if obj.image_names:
            # Print names of connected images
            print("\nImage names: {}".format(", ".join(obj.image_names)))

    def print_objects(self, obj_list=None):
        """Print all objects in 'obj_list' with their names, category entries and potential additional information.
        If 'obj_list' isn't specified, it will default to all listed objects."""
        if obj_list is None:
            obj_list = self.objects
        # Print objects with their positions in 'obj_list'
        for (i, obj) in enumerate(obj_list):
            print("\n{})".format(i + 1), end=" ")
            self._print_object(obj)

    def print_object_names(self, show_image_names=False):
        """Print list of object names. If 'show_image_names' equals True,
        also print names of respective connected images for each object."""
        if not show_image_names:
            print("\nObject names: {}".format(", ".join([obj.name for obj in self.objects])))
        else:
            # The column width used for the object names is determined by the longest name
            obj_name_col_width = max([len(obj.name) for obj in self.objects], default=0)
            print("\nObject names and names of connected images:")
            for obj in self.objects:
                print("- {:<{}}: {}".format(obj.name, obj_name_col_width, ", ".join(obj.image_names)))

    def print_category_names(self, only_calc_ctgrs=False):
        """Print list of category names. If 'only_calc_ctgrs' equals True, print only
        the names of categories that are used for aggregate/average calculation."""
        if not only_calc_ctgrs:
            print("\nCategory names: {}".format(", ".join(self.category_names)))
        else:
            calc_ctgrs = [ctgr_name for (ctgr_name, use_for_calc) in self.category_names.items() if use_for_calc]
            print("\nNames of calculation categories: {}".format(", ".join(calc_ctgrs)))

    def print_tags(self, show_obj_names=False):
        """Print list of tag names. If 'show_obj_names' equals True,
        also print names of respective tagged objects for each tag."""
        if not show_obj_names:
            print("\nTags: {}".format(", ".join(self.tags)))
        else:
            # The column width used for the tag names is determined by the longest name
            tag_name_col_width = max([len(tag) for tag in self.tags], default=0)
            print("\nTags and names of tagged objects:")
            for tag in self.tags:
                tagged_obj_names = [obj.name for obj in self.objects if tag in obj.tags]
                print("- {:<{}}: {}".format(tag, tag_name_col_width, ", ".join(tagged_obj_names)))

    def print_image_names(self, show_obj_names=False):
        """Print list of image names. If 'show_obj_names' equals True,
        also print names of respective connected objects for each image."""
        if not show_obj_names:
            print("\nImage names: {}".format(", ".join(self.images)))
        else:
            # The column width used for the image names is determined by the longest name
            image_name_col_width = max([len(image_name) for image_name in self.images], default=0)
            print("\nImage names and names of connected objects:")
            for image_name in self.images:
                connected_obj_names = [obj.name for obj in self.objects if image_name in obj.image_names]
                print("- {:<{}}: {}".format(image_name, image_name_col_width, ", ".join(connected_obj_names)))

    def get_object(self, obj_name, item_checker=standard_item_checker):
        """Return an object with the name 'obj_name' if such an object is listed. Otherwise return None."""
        obj_name = item_checker(obj_name, [obj.name for obj in self.objects], "object", True)
        if obj_name is not None:
            for obj in self.objects:
                if obj.name == obj_name:
                    return obj
        return None

    def add_object(self, obj_name, item_checker=standard_item_checker):
        obj_name = item_checker(obj_name, [obj.name for obj in self.objects], "object", False)
        if obj_name is not None:
            self.objects.append(ListedObject(obj_name, list(self.category_names)))

    def rename_object(self, obj_name, new_obj_name, item_checker=standard_item_checker):
        obj = self.get_object(obj_name, item_checker=item_checker)
        new_obj_name = item_checker(new_obj_name, [o.name for o in self.objects], "object", False,
                                    print_message=lambda x: obj is not None and x != obj.name)
        if obj is not None and new_obj_name is not None and obj.name != new_obj_name:
            obj.name = new_obj_name

    def delete_object(self, obj_name, item_checker=standard_item_checker):
        obj = self.get_object(obj_name, item_checker=item_checker)
        if obj is not None:
            self.objects.remove(obj)

    def add_category(self, ctgr_name, use_for_calc, item_checker=standard_item_checker):
        ctgr_name = item_checker(ctgr_name, self.category_names, "category", False)
        if ctgr_name is not None:
            self.category_names[ctgr_name] = use_for_calc
            for obj in self.objects:
                obj.ctgr_entries[ctgr_name] = ""

    def rename_category(self, ctgr_name, new_ctgr_name, item_checker=standard_item_checker):
        ctgr_name = item_checker(ctgr_name, self.category_names, "category", True)
        new_ctgr_name = item_checker(new_ctgr_name, self.category_names, "category", False,
                                     print_message=lambda x: ctgr_name is not None and x != ctgr_name)
        if ctgr_name is not None and new_ctgr_name is not None and ctgr_name != new_ctgr_name:
            self.category_names = OrderedDict([(new_ctgr_name if k == ctgr_name else k, v)
                                               for (k, v) in self.category_names.items()])
            for obj in self.objects:
                obj.ctgr_entries[new_ctgr_name] = obj.ctgr_entries.pop(ctgr_name)

    def delete_category(self, ctgr_name, item_checker=standard_item_checker):
        ctgr_name = item_checker(ctgr_name, self.category_names, "category", True)
        if ctgr_name is not None:
            use_for_calc = self.category_names.pop(ctgr_name)
            if use_for_calc:
                calc_ctgrs = [ctgr_name for (ctgr_name, ufc) in self.category_names.items() if ufc]
            for obj in self.objects:
                entry = obj.ctgr_entries.pop(ctgr_name)
                if use_for_calc and type(try_float_cast(entry)) is float:
                    # Aggregate and average values need to be re-calculated
                    obj.calculate_aggregate_average(calc_ctgrs)

    def edit_category_entry(self, obj_name, ctgr_name, new_entry, item_checker=standard_item_checker):
        obj = self.get_object(obj_name, item_checker=item_checker)
        ctgr_name = item_checker(ctgr_name, self.category_names, "category", True, print_message=lambda x: obj is not None)
        if obj is not None and ctgr_name is not None:
            old_entry = obj.ctgr_entries[ctgr_name]
            obj.ctgr_entries[ctgr_name] = new_entry
            use_for_calc = self.category_names[ctgr_name]
            if use_for_calc and any([type(try_float_cast(entry)) is float for entry in [old_entry, new_entry]]):
                # Aggregate and average need to be re-calculated
                calc_ctgrs = [ctgr_name for (ctgr_name, ufc) in self.category_names.items() if ufc]
                obj.calculate_aggregate_average(calc_ctgrs)

    def edit_category_order(self, ordered_ctgr_names):
        """Match the order of category names with the one of 'ordered_ctgr_names'
        if it's a list containing the same category names."""
        if len(ordered_ctgr_names) == len(self.category_names) and set(ordered_ctgr_names) == set(self.category_names):
            for ctgr_name in ordered_ctgr_names:
                self.category_names.move_to_end(ctgr_name)
        else:
            print("\nThis action would change more than just the category order.")

    def set_calculation_categories(self, calc_ctgrs, item_checker=standard_item_checker):
        """From now on, calculate aggregate and average values using the
        pure number entries of the categories specified in 'calc_ctgrs'."""
        calc_ctgrs = [ctgr_name for ctgr_name in map(lambda x: item_checker(x, self.category_names, "category", True),
                      calc_ctgrs) if ctgr_name is not None]
        for ctgr_name in self.category_names:
            self.category_names[ctgr_name] = ctgr_name in calc_ctgrs
        # Aggregate and average values need to be re-calculated for every object
        for obj in self.objects:
            obj.calculate_aggregate_average(calc_ctgrs)

    def add_tag(self, tag_name, item_checker=standard_item_checker):
        tag_name = item_checker(tag_name, self.tags, "tag", False)
        if tag_name is not None:
            self.tags.append(tag_name)

    def rename_tag(self, tag_name, new_tag_name, item_checker=standard_item_checker):
        tag_name = item_checker(tag_name, self.tags, "tag", True)
        new_tag_name = item_checker(new_tag_name, self.tags, "tag", False,
                                    print_message=lambda x: tag_name is not None and x != tag_name)
        if tag_name is not None and new_tag_name is not None and tag_name != new_tag_name:
            self.tags[self.tags.index(tag_name)] = new_tag_name
            for tagged_obj in [obj for obj in self.objects if tag_name in obj.tags]:
                tagged_obj.tags[tagged_obj.tags.index(tag_name)] = new_tag_name

    def delete_tag(self, tag_name, item_checker=standard_item_checker):
        tag_name = item_checker(tag_name, self.tags, "tag", True)
        if tag_name is not None:
            self.tags.remove(tag_name)
            for tagged_obj in [obj for obj in self.objects if tag_name in obj.tags]:
                tagged_obj.tags.remove(tag_name)

    def toggle_tag(self, obj_name, tag_name, item_checker=standard_item_checker):
        """Set or unset a tag for an object depending on the current state."""
        obj = self.get_object(obj_name, item_checker=item_checker)
        tag_name = item_checker(tag_name, self.tags, "tag", True, print_message=lambda x: obj is not None)
        if obj is not None and tag_name is not None:
            if tag_name in obj.tags:
                obj.tags.remove(tag_name)
            else:
                obj.tags.append(tag_name)

    def add_image(self, image_name, image_source, is_filepath, item_checker=standard_item_checker):
        """Load image from the 'image_source' and add it to list of images. If 'is_filepath' equals True,
        'image_source' is treated as a filepath. Otherwise it is treated as a URL."""
        image_name = item_checker(image_name, self.images, "image", False)
        if image_name is not None:
            if is_filepath:
                if os.path.isfile(image_source):
                    try:
                        image = Image.open(image_source)
                    except OSError:
                        print("\nFile couldn't be identified as an image.")
                        return
                else:
                    print("\nFile doesn't exist.")
                    return
            else:
                try:
                    request = requests.get(image_source)
                except requests.exceptions.RequestException:
                    print("\nAn error occurred while handling your request.")
                    return
                else:
                    try:
                        image = Image.open(io.BytesIO(request.content))
                    except OSError:
                        print("\nURL data couldn't be identified as an image.")
                        return
            with io.BytesIO() as byte_stream:
                image.save(byte_stream, image.format)
                image_bytes = byte_stream.getvalue()
            self.images[image_name] = image_bytes

    def rename_image(self, image_name, new_image_name, item_checker=standard_item_checker):
        image_name = item_checker(image_name, self.images, "image", True)
        new_image_name = item_checker(new_image_name, self.images, "image", False,
                                  print_message=lambda x: image_name is not None and x != image_name)
        if image_name is not None and new_image_name is not None and image_name != new_image_name:
            self.images = OrderedDict([(new_image_name if k == image_name else k, v)
                                       for (k, v) in self.images.items()])
            for connected_obj in [obj for obj in self.objects if image_name in obj.image_names]:
                connected_obj.image_names[connected_obj.image_names.index(image_name)] = new_image_name

    def delete_image(self, image_name, item_checker=standard_item_checker):
        image_name = item_checker(image_name, self.images, "image", True)
        if image_name is not None:
            del self.images[image_name]
            for connected_obj in [obj for obj in self.objects if image_name in obj.image_names]:
                connected_obj.image_names.remove(image_name)

    def toggle_image_connection(self, obj_name, image_name, item_checker=standard_item_checker):
        """Connect or disconnect an object and an image depending on the current state."""
        obj = self.get_object(obj_name, item_checker=item_checker)
        image_name = item_checker(image_name, self.images, "image", True, lambda x: obj is not None)
        if obj is not None and image_name is not None:
            if image_name in obj.image_names:
                obj.image_names.remove(image_name)
            else:
                obj.image_names.append(image_name)

    def display_image(self, image_name, item_checker=standard_item_checker):
        image_name = item_checker(image_name, self.images, "image", True)
        if image_name is not None:
            with io.BytesIO(self.images[image_name]) as byte_stream:
                Image.open(byte_stream).show()

    def display_connected_images(self, obj_name, item_checker=standard_item_checker):
        obj = self.get_object(obj_name, item_checker=item_checker)
        if obj is not None:
            for image_name in obj.image_names:
                with io.BytesIO(self.images[image_name]) as byte_stream:
                    Image.open(byte_stream).show()

    def display_all_images(self):
        for image_bytes in self.images.values():
            with io.BytesIO(image_bytes) as byte_stream:
                Image.open(byte_stream).show()

    def get_search_results(self, search_areas, search_ctgrs, search_term, strictness_level, obj_list=None):
        """Search for a 'search_term' in all specified 'search_areas' (possibilities: "Object names", "Category entries",
        "Tags", "Image names") of the objects in 'obj_list' and return the objects with matches as search results.
        If "Category entries" is a search area, the category names need to be specified in
        'search_ctgrs'. If 'obj_list' isn't specified, it will default to all listed objects.
        The 'strictness_level' can be set to the following integral values:
        - 0 (or less): Case-insensitive search for containing matches.
        - 1: Case-sensitive search for containing matches.
        - 2 (or greater): Case-sensitive search for exact matches."""
        def check_match(string):
            if strictness_level >= 2:
                # Check if string matches search term exactly
                return search_term == string
            elif strictness_level == 1:
                # Check if string contains search term (case-sensitive)
                return search_term in string
            else:
                # Check if string contains search term (case-insensitive)
                return search_term.lower() in string.lower()
        if obj_list is None:
            obj_list = self.objects
        # Search in each search area for the search term and update the set storing the object names of the results
        search_results = set()
        if "Object names" in search_areas:
            search_results.update([obj.name for obj in obj_list if check_match(obj.name)])
        if "Category entries" in search_areas:
            for ctgr_name in search_ctgrs:
                if ctgr_name in self.category_names:
                    search_results.update([obj.name for obj in obj_list if check_match(obj.ctgr_entries[ctgr_name])])
        if "Tags" in search_areas:
            search_results.update([obj.name for obj in obj_list if any([check_match(tag) for tag in obj.tags])])
        if "Image names" in search_areas:
            search_results.update([obj.name for obj in obj_list if any([check_match(image_name) for image_name
                                                                        in obj.image_names])])
        # Return a list of objects corresponding to the object names in the set of search results
        return [obj for obj in map(self.get_object, search_results) if obj is not None]

    def get_sorted_objects(self, sorting_ctgr_ids, obj_list=None):
        """Return a sorted version of 'obj_list' using 'sorting_ctgr_ids'.
        If 'obj_list' isn't specified, it will default to all listed objects.
        
        sorting_ctgr_ids (collections.OrderedDict):
        - Keys: Category indices derived from the list of category names, or the strings "Object name", "Aggregate" and "Average"
        - Values: Boolean values, which determine whether the corresponding sorting result is reversed or not
        - the order from beginning to end determines the priorities of the sorting categories from highest to lowest"""

        sorted_objects = obj_list if obj_list is not None else self.objects
        # Sort the objects repeatedly in order of reversed priority. The last sorting process (highest priority) determines
        # the general object order, but in case of ties the orders from previous sorting processes are remembered.
        for (sorting_ctgr_id, reverse) in reversed(sorting_ctgr_ids.items()):
            if type(sorting_ctgr_id) is int and sorting_ctgr_id in range(len(self.category_names)):
                ctgr_name = list(self.category_names)[sorting_ctgr_id]
                sorted_objects = sorted(sorted_objects, key=lambda obj: natural_key(obj.ctgr_entries[ctgr_name]),
                                        reverse=reverse)
            elif sorting_ctgr_id == "Object name":
                sorted_objects = sorted(sorted_objects, key=lambda obj: natural_key(obj.name), reverse=reverse)
            # For aggregate and average values, the sorting keys ensure that |None < any number| applies effectively
            elif sorting_ctgr_id == "Aggregate":
                sorted_objects = sorted(sorted_objects, key=lambda obj: (obj.aggr_avrg is not None, obj.get_aggregate()),
                                        reverse=reverse)
            elif sorting_ctgr_id == "Average":
                sorted_objects = sorted(sorted_objects, key=lambda obj: (obj.aggr_avrg is not None, obj.get_average()),
                                        reverse=reverse)
        return sorted_objects

    def print_chart(self, obj_list=None):
        """Print a comparison of all objects in 'obj_list' in the form of a chart.
        If 'obj_list' isn't specified, it will default to all listed objects."""
        if obj_list is None:
            obj_list = self.objects
        obj_col_widths = {obj.name: self._get_max_obj_length(obj) for obj in obj_list}
        ctgr_col_width = self._get_max_ctgr_name_length(include_aggr=any([obj.aggr_avrg is not None for obj in obj_list]))
        # Print the first row, which contains the object names
        offset = ctgr_col_width + 7 if self.display_aggr_avrg else ctgr_col_width + 5
        print("\n", end=" " * offset)
        for obj in obj_list:
            print("{:<{}}".format(obj.name, obj_col_widths[obj.name]), end=" " * 4)
        # Print the following rows, of which each one covers a category
        for (ctgr_name, use_for_calc) in self.category_names.items():
            print()
            if self.display_aggr_avrg:
                # Print calculation indicator
                calc_indicator = "*" if use_for_calc else "-"
                print(calc_indicator, end=" ")
            # Print category name
            print("{:<{}}:".format(ctgr_name, ctgr_col_width), end=" " * 4)
            # Print the category entry of each object with potential rank information
            for obj in obj_list:
                entry_output = obj.ctgr_entries[ctgr_name] + self._get_ctgr_entry_rank_info(obj, ctgr_name)
                print("{:<{}}".format(entry_output, obj_col_widths[obj.name]), end=" " * 4)
        if self.display_aggr_avrg and any([obj.aggr_avrg is not None for obj in obj_list]):
            # Collect aggregate and average outputs of each object
            aggr_outputs = {obj.name: "" for obj in obj_list}
            avrg_outputs = {obj.name: "" for obj in obj_list}
            for obj in obj_list:
                if obj.aggr_avrg is not None:
                    # Set aggregate and average outputs to the respective values with potential rank information
                    (aggr_rank_info, avrg_rank_info) = self._get_aggr_avrg_rank_info(obj)
                    aggr_outputs[obj.name] = str(obj.get_aggregate()) + aggr_rank_info
                    avrg_outputs[obj.name] = "{:.{}f}{}".format(obj.get_average(), self.avrg_decimal_places, avrg_rank_info)
            print()
            # Print one row for aggregate values and one for average values
            for (output_type, outputs) in [("Aggregate", aggr_outputs), ("Average", avrg_outputs)]:
                print("\n* {:<{}}:".format(output_type, ctgr_col_width), end=" " * 4)
                for obj in obj_list:
                    print("{:<{}}".format(outputs[obj.name], obj_col_widths[obj.name]), end=" " * 4)
        print()

    # Define equality behaviour to allow equality tests

    def __eq__(self, other):
        if isinstance(other, self.__class__):
            return self.__dict__ == other.__dict__
        else:
            return NotImplemented

    def __ne__(self, other):
        if isinstance(other, self.__class__):
            return not self == other
        else:
            return NotImplemented
